# Navel Config

Navel Config is a system for parsing configuration files.
It was built to suit the requirements of [Navel](../navel),
though it can be used as an independent module for any project.

## Installing

You can add this module to your node project with NPM:

```sh
npm install --save navel-config
```

## Usage

This section will explain how to use this library.

### Basic usage

To load a configuration file you use the module's `load` method.
The `load` method returns a [Future](https://github.com/fluture-js/Fluture) that
resolves to a `Config` instance.

```javascript
const Config = require('navel-config');
const ROOT_CONFIG_PATH = './config.yml';

Config.load(ROOT_CONFIG_PATH, process.env)
    .fork(console.error, (config) => {
        // config is a Config instance
    })
;
```

It will also load a remote resource:

```javascript
const Config = require('navel-config');
const ROOT_CONFIG_URL = 'https://example.com/config.json';

Config.load(ROOT_CONFIG_URL, process.env)
    .fork(console.error, (config) => {
        // config is a Config instance
    })
;
```

### File formats

This module will load files with a `.yml`, `.yaml` or `.json` file extension.  Additionally, it will load local files with a `.js` file extension.

### Environment variables

This module will replace any value prefixed with a `$` character with a value from the passed in environment.

```yaml
# config.yml
example:
  var: $EXAMPLE_VAR
```

```javascript
const ENV = {EXAMPLE_VAR: 'ok'};
Config.load(pathToConfig, ENV)
    .fork(console.error, (config) => {
        config.get('example/var'); // 'ok'
    })
;
```

There is also an operator that will achieve the same thing:

```yaml
# config.yml
example:
  var:
    $env: EXAMPLE_VAR
```

```javascript
const ENV = {EXAMPLE_VAR: 'ok'};
Config.load(pathToConfig, ENV)
    .fork(console.error, (config) => {
        config.get('example/var'); // 'ok'
    })
;
```

You can use a backslash to escape the environment variable:

```yaml
# config.yml
example:
  var: \$EXAMPLE_VAR
```

```javascript
const ENV = {EXAMPLE_VAR: 'ok'};
Config.load(pathToConfig, ENV)
    .fork(console.error, (config) => {
        config.get('example/var'); // '$EXAMPLE_VAR'
    })
;
```

### References

A configuration file can contain a [JSON reference](https://tools.ietf.org/html/draft-pbryan-zyp-json-ref-03).
This reference can be a local or remote reference.

```yaml
# config.yml
example:
  reference:
    $ref: 'https://example.com/config.yml#thing'
```

```yaml
# https://example.com/config.yml
thing: value
```

```javascript
Config.load(pathToConfig)
    .fork(console.error, (config) => {
        config.get('example/reference'); // 'value'
    })
;
```

### Case Expression

A configuration file may contain a case expression:

```yaml
# config.yml
example:
  $case:
    of: $color
    cases:
      blue: good
      red: bad
    default: neutral
```

```javascript
Config.load(pathToConfig, {color: 'red'})
    .fork(console.error, (config) => {
        config.get('example'); // 'bad'
    })
;
```

### Verbatim

If you want a section of content to be returned without processing,
you may use the `$verbatim` operator.

```yaml
literal:
  $verbatim:
    $ref: '#/reference'
    $env: 'var'
```

```javascript
Config.load(pathToConfig)
    .fork(console.error, (config) => {
        config.get('literal/$ref'); // '#/reference'
    })
;
```

## Running the tests

You can run the project tests with NPM:

```sh
npm run lint && npm test
```

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/wardlem/navel) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/wardlem/navel/tags).

## Authors

* **Mark Wardle** - *Initial work* - [wardlem](https://gitlab.com/wardlem)

## License

This project is licensed under the ISC License - see the [LICENSE](https://gitlab.com/warldem/navel/LICENSE) file for details

## Acknowledgments

Forthcoming.
