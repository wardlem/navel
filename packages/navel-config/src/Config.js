const {extname} = require('path');
const {readFile} = require('fs');
const {
    fileURLToPath,
    format: formatUrl,
    URL,
} = require('url');

const Future = require('fluture');
const {curry} = require('ramda');
const fetch = require('node-fetch');

const lazy$ = Symbol('navel/config/lazy');
const resolving$ = Symbol('navel/config/resolving');

const CACHE = new Map();

const SUPPORTED_PROTOCOLS = new Set(['file:', 'http:', 'https:']);
const SUPPORTED_EXTENSIONS = new Set(['.json', '.yml', '.yaml']);

function Config(data = {}, env = {}, rootpath = '/') {
    if (rootpath[0] !== '/') {
        rootpath = `/${rootpath}`;
    }

    return {
        get env() { return env; },
        get data() { return data; },
        get rootpath() { return rootpath; },
        __proto__: Config.prototype,
    };
}

Config.load = function load(rooturi, env = process.env) {
    rooturi = _uriToObject(rooturi);
    return Config._load(rooturi, env)
        .chain(Config.resolveLazyValues)
        .map((data) => {
            const path = rooturi.hash ? rooturi.hash.substr(1) : '/';
            data = _getPath(path, data);
            return Config(data, env, path);
        })
    ;
};

Config._load = function _load(rooturi, env) {
    rooturi = _uriToObject(rooturi);

    if (!SUPPORTED_PROTOCOLS.has(rooturi.protocol)) {
        return Future.reject(new Error(`Cannot load ${rooturi} because the protocol ${rooturi.protocol} is not supported`));
    }

    const uristr = formatUrl(rooturi, {fragment: false});
    if (CACHE.has(uristr)) {
        return Future.of(CACHE.get(uristr));
    }

    return Config.loadUri(rooturi)
        .chain(Config.processData(rooturi, env))
        .map(saveFileCache(rooturi))
    ;
};

Config.loadUri = function loadUri(uri) {
    uri = _uriToObject(uri);
    if (uri.protocol === 'file:') {
        return Config.loadFile(fileURLToPath(uri));
    }

    return Config.loadUrl(formatUrl(uri, {fragment: false}));
};

Config.loadUrl = function loadUrl(url) {
    const ext = extname(url);

    if (!SUPPORTED_EXTENSIONS.has(ext)) {
        return Future.reject(new Error(`${url} has an unsupported file extension`));
    }

    return Future.tryP(() => fetch(url))
        .chain((response) => {
            if (!response.ok) {
                return Future.reject(new Error(`Could not load file at ${url}`));
            }

            // TODO: read content-type header for ext

            return Future.tryP(() => response.buffer());
        })
        .chain(parseFile(ext))
    ;
};

Config.loadFile = function loadFile(path) {
    const ext = extname(path);

    if (ext === '.js' || ext === '.node') {
        return Future.try(() => require(path));
    } else if (SUPPORTED_EXTENSIONS.has(ext)) {
        return Future.node((cb) => readFile(path, cb))
            .chain(parseFile(ext))
        ;
    } else {
        // TODO: make this a better error type
        return Future.reject(new Error(`File at path ${path} has an unsupported file extension ${ext}`));
    }
};

Config.processData = curry(function processData(rooturi, env, data) {
    return Future.try(_processData.bind(null, data, rooturi, [], env));
});

Config.resolveLazyValues = curry(function resolveLazyValues(data) {
    if (data == null) {
        return Future.of(data);
    } else if (Array.isArray(data)) {
        return Future.parallel(1, data.map(Config.resolveLazyValues));
    } else if (data[lazy$]) {
        if (data[resolving$]) {
            return Future.reject(new Error(`circular reference in configuaration file ${data.rooturi}#/${data.path.join('/')}`));
        }
        data[resolving$] = true;
        return data.load.chain(Config.resolveLazyValues).map((value) => {
            data[resolving$] = false;
            return value;
        });
    } else if (data.constructor === Object) {
        return Future.parallel(1, Object.keys(data).map((key) => {
            return Config.resolveLazyValues(data[key]).map((value) => [key, value]);
        })).map((entries) => {
            return entries.reduce((res, [key, value]) => {
                res[key] = value;
                return res;
            }, {});
        });
    } else {
        return Future.of(data);
    }
});

Config.clearCache = function clearCache() {
    CACHE.clear();
};

Config.prototype.get = function(path) {
    return _getPath(path, this.data);
};

function _processData(data, rooturi, path, env) {

    if (data == null) {
        return data;
    }

    if (Array.isArray(data)) {
        return data.map((v, idx) => _processData(v, rooturi, path.concat(idx), env));
    }

    if (data.constructor === Object) {
        const keys = Object.keys(data);
        return keys.reduce((res, key) => {
            if (key[0] === '$') {
                if (keys.length !== 1) {
                    throw new Error(`Operator object at ${path.join('/')} has multiple keys, but only one is allowed`);
                }

                return _processOperator(key, data[key], rooturi, path, env);
            } else if (key[0] === '\\' && key[1] === '$') {
                key = key.substr(1);
            }

            res[key] = _processData(data[key], rooturi, path.concat(key), env);
            return res;
        }, {});
    } else if (typeof data === 'string' && data[0] === '$') {
        return _processEnvOperator(data.substr(1), env);
    } else if (typeof data === 'string' && data[0] === '\\' && data[1] === '$') {
        // Escaped env variable
        return data.substr(1);
    }
    return data;
}

function _processOperator(operator, value, rooturi, path, env) {
    switch (operator) {
        case '$ref':
            return _processRefOperator(value, rooturi, path, env);
        case '$env':
            return _processEnvOperator(value, env);
        case '$case':
            return _processCaseOperator(value, rooturi, path, env);
        case '$verbatim':
            return value;
        default:
            throw new Error(`Operator ${operator} at path ${path.join('.')} is not supported`);
    }
}


function _processRefOperator(value, rooturi, path, env) {
    let uri;
    try {
        uri = new URL(value, rooturi);
    } catch (e) {
        throw new Error(`Reference value ${value} at path ${path.join('.')} is invalid`);
    }

    return {
        [lazy$]: true,
        rooturi,
        path,
        load: Config._load(uri, env).chain((res) => {
            const pathParts = _splitPath(uri.hash && uri.hash.substr(1) || '/');

            return pathParts.reduce((resf, key) => {
                return resf.chain((res) => {
                    // TODO: should we throw an error for an unresolvable path instead?
                    if (res == null) {
                        return Future.of(undefined);
                    }

                    const out = res[key];
                    if (out == null) {
                        return Future.of(out);
                    }

                    if (out[lazy$]) {
                        if (out[resolving$]) {
                            return Future.reject(new Error(`circular reference in configuration file ${out.rooturi}#/${out.path.join('/')}`));
                        }
                        return out.load;
                    }

                    return Future.of(out);
                });
            }, Future.of(res));
        }),
    };
}

function _processEnvOperator(key, env) {
    if (typeof key !== 'string') {
        throw new TypeError(`$env key must be a string but got ${key == null ? String(key) : typeof key}`);
    }

    return _getPath(key, env);
}

function _processCaseOperator(def, rooturi, path, env) {
    if (def == null || typeof def !== 'object') {
        throw new TypeError('$case operator expects an object as a definition');
    }

    const {
        of,
        cases,
    } = def;

    if (of == null) {
        throw new TypeError('$case operator expects an `of` clause in the definition');
    }

    if (cases == null) {
        throw new TypeError('$case operator expects a `cases` clause in the definition');
    }

    const ofFuture = Future.try(_processData.bind(null, of, rooturi, path.concat(['$case', 'of']), env))
        .chain(Config.resolveLazyValues)
    ;
    const casesFuture = Future.try(_processData.bind(null, cases, rooturi, path.concat(['$case', 'cases']), env));
    const elseFuture = Future.try(_processData.bind(null, def.default, rooturi, path.concat(['$case', 'else']), env));
    return {
        [lazy$]: true,
        rooturi,
        path,
        load: Future.parallel(1, [ofFuture, casesFuture]).chain(([of, cases]) => {
            if (cases != null && typeof cases === 'object') {
                if (cases[lazy$]) {
                    return Config.resolveLazyValues(cases)
                        .chain((cases) => {
                            if (cases == null || typeof cases !== 'object') {
                                return elseFuture
                            }

                            if (cases.hasOwnProperty(of)) {
                                return Future.of(cases[of])
                            }

                            return elseFuture;
                        })
                    ;
                } else if (cases.hasOwnProperty(of)) {
                    return Config.resolveLazyValues(cases[of])
                }
            }

            return elseFuture;
        }),
    };
}

function _splitPath(string) {
    if (string[0] === '/') {
        string = string.substr(1);
    }

    if (string === '') {
        return [];
    }

    return string.split('/').map((part) => part.replace('~1', '/').replace('~0', '~'));
}

function _getPath(path, obj) {
    const parts = _splitPath(path);

    let v = obj;
    for (let part of parts) {
        v = part == null ? undefined : v[part];
    }

    return v;
}

function _uriToObject(uri) {
    if (!(uri instanceof URL)) {
        try {
            uri = new URL(uri, `file://${process.cwd()}`);
        } catch (e) {
            throw new Error(`${uri} is not a valid path`);
        }
    }

    return uri;
}

const parseFile = curry(function parseFile(ext, buffer) {
    if (ext === '.json') {
        return Future.try(() => JSON.parse(buffer));
    } else if (ext === '.yml' || ext === '.yaml') {
        const YAML = require('js-yaml');
        return Future.try(() => YAML.safeLoad(buffer));
    }

    return Future.reject(new Error(`Extension ${ext} is not supported`));
});

const saveFileCache = curry(function saveFileCache(uri, data) {
    const uristr = formatUrl(uri, {fragment: false});
    CACHE.set(uristr, data);
    return data;
});

module.exports = Config;
