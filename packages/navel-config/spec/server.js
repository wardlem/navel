const http = require('http');
const express = require('express');
const {resolve} = require('path');

const app = express();
app.use('/files', express.static(resolve(__dirname, './data')));
const server = http.createServer(app).listen(5432);

module.exports = server;
