const Config = require('../src/Config');
const {resolve} = require('path');

let server;
beforeAll(() => {
    server = require('./server');
});

afterAll(() => {
    server.close();
});

beforeEach(() => {
    Config.clearCache();
});

afterEach(() => {
    jest.clearAllMocks();
});

describe('Config', () => {
    it('is a constructor function', () => {
        expect(typeof Config).toEqual('function');
        expect(Config() instanceof Config).toBe(true);
    });

    it('sets default values', () => {
        const config = Config();
        expect(config.env).toEqual({});
        expect(config.data).toEqual({});
        expect(config.rootpath).toEqual('/');
    });

    describe('.load', () => {
        it('reads a local yaml file', (done) => {
            Config.load(resolve(__dirname, './data/simple.yml'))
                .fork(done, (config) => {
                    expect(config.data.key).toEqual('yaml');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('reads a local json file', (done) => {
            Config.load(resolve(__dirname, './data/simple.json'))
                .fork(done, (config) => {
                    expect(config.data.key).toEqual('json');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('reads a local js file', (done) => {
            Config.load(resolve(__dirname, './data/simple.js'))
                .fork(done, (config) => {
                    expect(config.data.key).toEqual('js');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('reads a remove yaml file', (done) => {
            Config.load('http://localhost:5432/files/simple.yml')
                .fork(done, (config) => {
                    expect(config.data.key).toEqual('yaml');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('reads a remote json file', (done) => {
            Config.load('http://localhost:5432/files/simple.json')
                .fork(done, (config) => {
                    expect(config.data.key).toEqual('json');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('resolves self references', (done) => {
            Config.load(resolve(__dirname, './data/selfref.yml'))
                .fork(done, (config) => {
                    expect(config.data.thing1.a).toEqual('b');
                    expect(config.data.thing2.c).toEqual('b');
                    done();
                })
            ;
        });

        it('resolves an array of self references', (done) => {
            Config.load(resolve(__dirname, './data/arrayref.yml'))
                .fork(done, (config) => {
                    expect(config.data.thing2[0]).toEqual(1);
                    expect(config.data.thing2[1]).toEqual(2);
                    done();
                })
            ;
        });

        it('resolves recursive self references', (done) => {
            Config.load(resolve(__dirname, './data/recursiveref.yml'))
                .fork(done, (config) => {
                    expect(config.data.thing.a).toEqual('yes');
                    expect(config.data.thing.b).toEqual('yes');
                    expect(config.data.thing.c).toEqual('yes');
                    done();
                })
            ;
        });

        it('resolves an environment reference', (done) => {
            const env = {
                thing1: {
                    a: 'awesome',
                },
            };

            Config.load(resolve(__dirname, './data/envref.yml'), env)
                .fork(done, (config) => {
                    expect(config.data.a).toEqual('awesome');
                    done();
                })
            ;
        });

        it('uses process.env by default when resolving an environment reference', (done) => {
            const oldEnv = process.env;
            process.env = {
                thing1: {
                    a: 'awesome',
                },
            };

            Config.load(resolve(__dirname, './data/envref.yml'))
                .fork(done, (config) => {
                    expect(config.data.a).toEqual('awesome');

                    process.env = oldEnv;
                    done();
                })
            ;
        });

        it('resolves a shorthand environment reference', (done) => {
            const env = {
                thing1: {
                    a: 'awesome',
                },
            };

            Config.load(resolve(__dirname, './data/envref.yml'), env)
                .fork(done, (config) => {
                    expect(config.data.b).toEqual('awesome');
                    done();
                })
            ;
        });

        it('allows a shorthand environment variable to be escaped', (done) => {
            const env = {
                thing1: {
                    a: 'awesome',
                },
            };

            Config.load(resolve(__dirname, './data/envref.yml'), env)
                .fork(done, (config) => {
                    expect(config.data.c).toEqual('$thing1/a');
                    done();
                })
            ;
        });

        it('executes a case operator', (done) => {
            Config.load(resolve(__dirname, './data/simplecase.yml'), {color: 'red'})
                .fork(done, (config) => {
                    expect(config.data).toEqual({
                        test: 'bad',
                    });
                    Config.clearCache();
                    Config.load(resolve(__dirname, './data/simplecase.yml'), {color: 'blue'})
                        .fork(done, (config) => {
                            expect(config.data).toEqual({
                                test: 'good',
                            });
                            Config.clearCache();
                            Config.load(resolve(__dirname, './data/simplecase.yml'), {color: 'green'})
                                .fork(done, (config) => {
                                    expect(config.data).toEqual({
                                        test: 'neutral',
                                    });
                                    done();
                                })
                            ;
                        })
                    ;
                })
            ;
        });

        it('executes a more complex case operator', (done) => {
            Config.load(resolve(__dirname, './data/complexcase.yml'), {color: 'red'})
                .fork(done, (config) => {
                    expect(config.data.test).toEqual('bad');
                    Config.clearCache();
                    Config.load(resolve(__dirname, './data/complexcase.yml'), {color: 'blue'})
                        .fork(done, (config) => {
                            expect(config.data.test).toEqual('good');
                            Config.clearCache();
                            Config.load(resolve(__dirname, './data/complexcase.yml'), {color: 'green'})
                                .fork(done, (config) => {
                                    expect(config.data.test).toEqual('neutral');
                                    done();
                                })
                            ;
                        })
                    ;
                })
            ;
        });

        it('returns the contents of a verbatim operator as is', (done) => {
            Config.load(resolve(__dirname, './data/verbatim.yml'))
                .fork(done, (config) => {
                    expect(config.data.literal).toEqual({
                        $ref: '#/reference',
                        $env: 'var',
                    });
                    done();
                })
            ;
        });

        it('fails when an invalid opcode is found', (done) => {
            Config.load(resolve(__dirname, './data/bad.yml'))
                .fork((err) => {
                    expect(err instanceof Error).toBe(true);
                    expect(err.message).toMatch(/\$badop at path a.b is not supported/i);
                    done();
                }, (config) => {
                    throw new Error('Should have failed');
                })
            ;
        });

        it('fails when more than one opcode is found', (done) => {
            Config.load(resolve(__dirname, './data/badtwoops.yml'))
                .fork((err) => {
                    expect(err instanceof Error).toBe(true);
                    expect(err.message).toMatch(/one is allowed/i);
                    done();
                }, (config) => {
                    throw new Error('Should have failed');
                })
            ;
        });

        it('fails when a circular reference is found', (done) => {
            Config.load(resolve(__dirname, './data/badcircular.yml'))
                .fork((err) => {
                    expect(err instanceof Error).toBe(true);
                    expect(err.message).toMatch(/circular reference/i);
                    done();
                }, (config) => {
                    throw new Error('Should have failed');
                })
            ;
        });

        it('loads a path when a hash is provided', (done) => {
            Config.load(resolve(__dirname, './data/simple.yml#key'))
                .fork(done, (config) => {
                    expect(config.rootpath).toEqual('/key');
                    expect(config.data).toEqual('yaml');
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('treats an empty path the same as no path', (done) => {
            Config.load(resolve(__dirname, './data/simple.yml#'))
                .fork(done, (config) => {
                    expect(config.rootpath).toEqual('/');
                    expect(config.data).toEqual({key: 'yaml'});
                    expect(config instanceof Config).toBe(true);
                    done();
                })
            ;
        });

        it('caches a file path', (done) => {
            const uri = `file://${resolve(__dirname, './data/simple.yml')}`;
            const spy = jest.spyOn(Config, 'loadFile');
            Config.load(uri)
                .fork(done, (config1) => {
                    expect(spy).toHaveBeenCalled();
                    spy.mockReset();
                    Config.load(uri)
                        .fork(done, (config2) => {
                            expect(spy).not.toHaveBeenCalled();
                            expect(config1).toEqual(config2);
                            spy.mockRestore();
                            done();
                        })
                    ;
                })
            ;
        });

        it('caches a url', (done) => {
            const uri = 'http://localhost:5432/files/simple.json';
            const spy = jest.spyOn(Config, 'loadUrl');
            Config.load(uri)
                .fork(done, (config1) => {
                    expect(spy).toHaveBeenCalled();
                    spy.mockReset();
                    Config.load(uri)
                        .fork(done, (config2) => {
                            expect(spy).not.toHaveBeenCalled();
                            expect(config1).toEqual(config2);
                            spy.mockRestore();
                            done();
                        })
                    ;
                })
            ;
        });

        it('errors if an unsupported protocol is used', (done) => {
            Config.load('ftp://localhost:5432/files/simple.json')
                .fork((err) => {
                    expect(err.message).toMatch(/the protocol ftp: is not supported/i);
                    done();
                }, (config) => {
                    done(new Error('should have failed'));
                })
            ;
        });

        it('errors if an unsupported extension is used in a file path', (done) => {
            Config.load(resolve(__dirname, './data/simple.php'))
                .fork((err) => {
                    expect(err.message).toMatch(/unsupported file extension/i);
                    done();
                }, (config) => {
                    done(new Error('should have failed'));
                })
            ;
        });

        it('errors if an unsupported extension is used in a url', (done) => {
            Config.load('https://localhost:5432/files/simple.php')
                .fork((err) => {
                    expect(err.message).toMatch(/unsupported file extension/i);
                    done();
                }, (config) => {
                    done(new Error('should have failed'));
                })
            ;
        });

        it('errors if a url returns a bad response code', (done) => {
            Config.load('http://localhost:5432/files/notfound.json')
                .fork((err) => {
                    expect(err.message).toMatch(/could not load file/i);
                    done();
                }, (config) => {
                    done(new Error('should have failed'));
                })
            ;
        });
    });

    describe('#get', () => {
        it('gets a value at a path', (done) => {
            Config.load(resolve(__dirname, './data/arrayref.yml'))
                .fork(done, (config) => {
                    expect(config.get('thing2/0')).toEqual(1);
                    expect(config.get('thing2/1')).toEqual(2);
                    done();
                })
            ;
        });
    });
});
